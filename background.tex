
\chapter{Background}\label{C:bk}

In 2001 and 2002 Dean Armstrong created the REX boards \cite{pearson2002design}\cite{pearson2002using} and the
\gls{wramp} CPU as a teaching platform
for \gls{comp200}. The REX boards were populated with four \glspl{fpga} to manage the various functions of the system.  There have since been other projects that have focused on adapting the original \gls{ip} cores of the four \glspl{fpga} into a design that uses only one \gls{fpga}, so that it can be replaced with a more readily available development kit.  This chapter first details the \gls{ip} cores that the new development kit will use.  It then mentions the open source hardware that had a large influence on the design of the boards, and finishes off covering the software used for the board's tool chain.


\section{IP cores}
In digital electronic design, an \gls{ip} core, or \gls{ip} block, is a reusable unit of logic, cell, or chip layout design that is the intellectual property of one party.  \gls{ip} cores can be used for both \gls{fpga} and \gls{asic} design.  This project uses many cores that are written in \gls{hdl}, using both Verilog and VHDL.  The name VHDL is an acronym meaning VHSIC HDL, which in-turn means \textit{Very High Speed Integrated Circuit Hardware Description Language}\textemdash a real mouthful of a name!  

This project uses IP cores written in \gls{hdl} to configure the \gls{fpga} logic and make the REX/\gls{wramp} shield function.  Unfortunately the Mojo IDE~\cite{mojo} mentioned later in this chapter is not compatible with VHDL and therefore could not be used for this project.  However the Mojo IDE can still be used for Verilog \gls{ip} cores that have been written for the platform.

Dean Armstrong created the \gls{ip} cores for the \gls{wramp} as well as several additional \gls{ip} cores for the peripherals on the REX board.  The extra \gls{ip} cores included are required to control the three \glspl{fpga} that are used in addition to the main \gls{wramp} \gls{fpga} so that they can provide control and interrupt functionality  for the peripherals and a timer via \gls{mmio}.
The original \gls{ip} core for the parallel port also included logic to deal with the loading of the \gls{wrampmon} boot-loader program into the \gls{rom} chips, this core was removed from the design as the new board components made it redundant.

For COMP390-14A, a Verilog implementation of a terminal was created by the author. This terminal supports a subset of escape codes from the VT100 series of terminals.  The subset consists of the escape codes required to run the game written in \gls{wramp} assembly.  The \gls{ip} core makes use of a 15-pin D-subminiature connector to communicate with a \gls{vga} monitor and a 6-pin Mini-DIN connector as a \gls{ps2} keyboard connector.  It also uses serial communication to perform standard terminal functions.  During the development of this \gls{ip} core, a RS232 \gls{pmod} was used, however this project will use the \gls{ip} core with its serial port connected inside the \gls{fpga} to the serial port of the \gls{wramp} core.  This makes the new platform more student friendly and accessible by removing the need to have a physical terminal machine in order to use the hardware away from the labs for \gls{comp200} assignments that require a terminal.  Instead a \gls{ps2} keyboard and a \gls{vga} monitor can be plugged into the connectors on the \gls{wramp} shield as a substitute terminal.


\section{Open Source Hardware}
With no clear definition of what \gls{osh} is, Erik Rubow has suggested~\cite{rubow2008open} that one reasonable definition is found in the TAPR Open Hardware License~\cite{tapr}:

\textit{Open Hardware is a thing - a physical artifact, either electrical or mechanical
- whose design information is available to, and usable by, the public in a way
that allows anyone to make, modify, distribute, and use that thing.}
\\
This hardware project has taken inspiration from \gls{osh} by using \gls{osh} \gls{eda} tools as well as incorporating \gls{osh} architecture and ideas.  This section details some of the \gls{osh} projects that have had an influence. 



\subsection{Arduino, Shields and PMODs}
The Arduino~\cite{arduinoweb} is brand of \gls{osh} development boards.  Each model of Arduino consists of a microcontroller connected to some female headers and a \gls{usb} connector for communication with a PC.  The development boards are compatible with an open source \gls{ide} and language that the creators have made.  The Arduino language and \gls{ide} are born from two previous projects. The first project was \emph{Processing} \textemdash an easy to use programming language for beginners who did not know how to code.  The second project was called \emph{Wiring} \textemdash an \gls{ide} and \gls{gui} for making circuit boards.  As for the weird name, David Kushner~\cite{kushner2011making} reveals that it is named after a pub called `Bar di Re Arduino' that one of the creators wanted to honor. 
\begin{figure}[H]  
\begin{center}  
\includegraphics[width=6in]{google_shield.eps}  
\caption[A selection of Arduino shields. Source : Google image search for images with a reuse license.]{\small \sl A selection of Arduino shields. Source : Google image search for images with a reuse license.\label{fig:google_shield}}
\end{center}  
\end{figure}
A key concept of Arduino boards is the concept of a shield.  A shield is a separate circuit that can be connected to an Arduino board via header pins.  Shields can consist of any hardware a user may wish to attach to an Arduino provided the connecting pins that plug into the arduino are in the correct place and type --- i.e. a power pin is not connected to a ground pin.  As long as the connecting circuit is pin compatible, a developer is free to build any circuit that they wish to connect to the Arduino microcontroller.  In Figure \ref{fig:google_shield} you can see a small selection of shields that people have created,  from shields as simple as a breadboard to something more advanced like an Ethernet controller --- if it is a circuit you wish to attach to a microcontroller, it can be made as a shield.

A \gls{pmod} is the name for circuits with a six pin header that can then be used to connect to Digilent inc. \gls{fpga} development boards.  Before any component selection choices were made, the various \gls{ip} cores were tested on the Mojo board.  A RS232 \gls{pmod} was connected to the Mojo board to evaluate the number of slices needed for the Verilog Terminal, however a PMOD is not required for the hardware created for this project. 
\begin{figure}[H]  
\begin{center}  
\includegraphics[width=6in]{pmod.eps}  
\caption[The Digilent Inc. RS232 \gls{pmod} that was used for testing the Verilog terminal on the Mojo board.  Source : Digilent Inc.]{\small \sl The Digilent Inc. RS232 \gls{pmod} that was used for testing the Verilog terminal on the Mojo board.  Source : Digilent Inc.\label{fig:pmod}}  
\end{center}  
\end{figure}
\subsection{The Mojo board}
An interesting \gls{osh} development board that takes inspiration from the Arduino has been created by Embedded Micro.  This board is called the Mojo and is sold via the Embedded Micro website as well as through hobbyist suppliers like Sparkfun.  Embedded Micro points out that whilst \glspl{fpga} are great for many projects, they usually lack the ability to perform \gls{adc} tasks and even though Arduino based boards can perform \gls{adc},  they lack the ability of \glspl{fpga} \textemdash you can only write software for the microcontroller on an Arduino.  Embedded Micro has come up with a solution; the Mojo is a hybrid that incorporates both an Arduino compatible microcontroller for performing \gls{adc} and running tasks that are more suited to a microcontroller and also includes a powerful \gls{fpga} for digital tasks that are better for running on the \gls{fpga} than as software.  The Mojo also uses the concept of shields to facilitate external circuits in the same way Arduino does.  The Mojo provides 84 digital \glspl{io} for the \gls{fpga} on two 50-pin headers and 8 \gls{adc} \glspl{io} for the microcontroller on a single 10-pin header.  The microcontroller has a serial port connected to the \gls{fpga} and with the default boot loader it can provide the \gls{fpga} access to the \gls{usb} port communications that are connected directly to the microcontroller.

It was decided that the Mojo would be an ideal piece of \gls{osh} to model the project on.  The REX/WRAMP peripherals and memory could be located on a shield and the large \gls{fpga} can be used to drive the shield \textemdash being of a large enough size that the \gls{wramp} and the Verilog terminal \gls{ip} cores can both fit into the fabric.   Suitability of the Mojo was first evaluated by testing the Verilog terminal on the Mojo and the \gls{wramp} core was generated for the Spartan-6 architecture to ensure there was enough fabric space to hold all the \gls{ip} cores.

\section{Toolchain}
There is a toolchain that already exists for the \gls{wramp} as well as the Mojo board.  To minimise the work required to implement a new piece of hardware, it was decided to make the new designs compatible with the existing toolchain of the two projects \textemdash and where possible have the architecture of the boards the same as the originals to minimise the amount of editing that the \gls{hdl} would require.  

The \gls{wramp} toolchain should work as normal and care has been taken to keep the Jolly Logic design as close to the Mojo as possible so that the tools created by Embedded Micro also work.  Most importantly the Mojo Loader which is a small utility that loads a design for the \gls{fpga} into the \gls{rom} and onto the \gls{fpga}.  Whilst the Mojo Loader will not be required when using the \gls{wramp} tools, it is required when doing other projects that involve loading custom \gls{hdl}.  Also the Arduino \gls{ide} and the Mojo IDE can be used with the board when a user wants to make use of the microcontroller in a project.  