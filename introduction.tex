\chapter{Introduction}\label{C:intro}
The University of Waikato currently uses a \gls{fpga} based board and \gls{risc} \gls{cpu} as the teaching platform for assembly language in \gls{comp200}.  The REX board also has  \gls{ip} cores that have been written in a \gls{hdl} to provide the \gls{risc} \gls{cpu} access to a small selection of peripherals.  The \gls{risc} \gls{cpu} is called the \gls{wramp}.  The key features of the \gls{wramp} and the \gls{io} peripherals on the REX board are:
\begin{itemize}
\item \gls{mmio} to control and use two serial ports.
\item \gls{mmio} for eight switches.
\item \gls{mmio} for two buttons.
\item \gls{mmio} for two seven segment displays.
\item \gls{mmio} to control and use a timer.
\item A \gls{lcd} driver to provide debug information on a small \gls{lcd} screen.
\item Buttons and switches to control debugging.
\item 256K of \gls{rom} to hold the \gls{wrampmon} software.
\item 128K of \gls{ram} for \gls{wramp} system memory and user programs.

\end{itemize}

\section{Motivations}
Unfortunately the majority of the original boards that were created have aged and become faulty. There are also only four of the physical VT320 terminals available for interacting with the serial ports on the REX boards.  Even though it is possible to obtain replacement boards, the cost of these replacements is
very expensive by modern terms\textemdash a recent quote for replacements was \$1400.00 NZD per board. Using newer technology and a different hardware architecture, a more modern design can be created that is less than 10\% of the replacement costs of the REX boards.  This design can also be of a more portable size which would enable students the ability to work on assignments at home. 

The primary goal of this project was to create a suitable replacement for the REX board whilst maintaining backwards compatibility with the existing \gls{wramp} \gls{mmio} and also the terminal escape codes used by the game in the final assignment for \gls{comp200}.  A secondary goal was to make the new design more generic so that it can be used for teaching other papers like \gls{comp311} and doing projects\textemdash rather than it only being useful for \gls{comp200}.  Two \glspl{pcb} were to be created, one is a clone of an existing board (The Mojo), called the Jolly Logic, the other is a shield for the Jolly Logic, called the REX/WRAMP shield.



\section{Design Considerations}
A few considerations needed to be taken into account when designing the new hardware.  The original \gls{ip} cores were designed to be mapped to four \glspl{fpga} and needed to be merged into a design that was suitable for loading onto a single \gls{fpga} in order to lower the costs of production.  The new resulting \gls{ip} core would be quite large and estimates needed to be made on what size \gls{fpga} would be required to host the \gls{ip} cores.  The new \gls{ip} cores and hardware needed to provide the key features of the \gls{wramp} and the REX board.  

In addition to meeting the requirements of compatibility with the legacy platform, a secondary goal was to have a design that could be used in a wider range of papers and projects.  To make the board more viable for papers like \gls{comp311} it would need to be cheaper than existing hardware used and potentially have something more to offer.  The new design meets this secondary goal by incorporating the following features:

\begin{itemize}
\item The \gls{cpi} is lower than standard development kits by being physically small and utilizing offshore manufacturing coupled with open source tools and designs.
\item The board is more generic by requiring shields or breadboards for project peripherals, connectors and other devices.
\item The board incorporates an Arduino compatible microchip for Analog \gls{io} and an \gls{fpga} for digital \gls{io} to make the board useful in a wider range of projects than an Arduino based or \gls{fpga} based board offers.
\item In order to meet the requirements of hosting all the \gls{ip} cores needed for the \gls{wramp}, the board has a much larger sized \gls{fpga} than the standard entry level \gls{fpga} development boards.
\item The Spartan-6 \gls{fpga} used for the board has a lot more resources than the Spartan-3 \glspl{fpga} that are used on many entry level \gls{fpga} development boards.
\end{itemize}

Care was also taken to keep to the same form factor and pinout as the Mojo.  This makes shields compatible with both the Mojo and the Jolly Logic, enabling the Mojo to be used as a base case when performing evaluation of a shield on the Jolly Logic prototype.
\section{Architecture Changes}
Improvements and modifications to the REX board and \gls{wramp} architecture other than merging the original \gls{ip} cores into a design based on a single \gls{fpga} were made.  This section covers the key changes to the \gls{hdl} and physical architecture.

A basic \gls{hdl} implementation of a terminal is used to replace one of the serial ports and the need for a physical terminal.  By using connectors for a \gls{vga} monitor and a \gls{ps2} keyboard in place of the terminal and moving the serial communications with the terminal to inside the \gls{fpga}, the serial port designated for the terminal can be removed\textemdash replacing it with a \gls{vga} and \gls{ps2} connector.  In addition to removing the serial port used for the terminal, the serial port that communicates with the PC will be replaced by using the microcontroller as a proxy for the \gls{usb} connection with a PC\textemdash replacing the need for an RS232 connector and serial cable with a more common usb cable.

 

The switches and buttons along with the \gls{lcd} display that is used for debugging and the associated \gls{hdl} have been removed from the design.  This is because the \gls{lcd} Display was intended to show the user information about the current address and data, however due to the \gls{wrampmon} program running underneath the user programs in order to facilitate the debugging, it has the unwanted side effect of making the display data change between the two memory spaces and effectively becomes meaningless data.  Another driving factor in removing this feature was the need to limit the digital \glspl{io} used by the \gls{wramp} to the 84 \glspl{io} available on the headers of the Jolly Logic.  With this to consider it was decided that removal of the feature was a better option than spending time on creating a fix so that the debugging tools worked as intended.

Lastly, when Dean Armstrong wrote the \gls{ip} core for the REX board parallel port, it included logic to deal with programming \gls{wrampmon} into the \gls{rom} memory.  Because the type of memories and the architecture has changed, this was removed from the parallel port \gls{ip} core.

\section{Structure}
This report details how the new hardware was developed and what \gls{ip} cores are required for the hardware to function.  The second chapter in this report covers background work and talks about hardware, \gls{ip} cores, and the software tools that influenced this project.  The third chapter is on the design method and talks about component selection and what was learned during the design process.  The fourth chapter is about the hardware that was created, it first covers the specifications of the two boards that were created and then goes on to cover the lessons learned about manufacturing and prototype assembly.  The final chapter in this report is on the results of the project and contains some suggestions for future work.
