

\chapter{Hardware}
Two different \glspl{pcb} were created for this project.  The first was a clone of an existing piece of \gls{osh} that is a cross between an Arduino and a \gls{fpga} development kit, called the Mojo.  The second was a shield for the first board that incorporated the memories and peripherals that are required to support the \gls{wramp} used in \gls{comp200}.  This chapter details the specifications of each board then covers the lessons learned during the manufacturing process of the prototypes.
\begin{figure}[H]  
\begin{center}  
\includegraphics[width=6in]{jolly_bot.eps}  
\caption[The Jolly Logic, as seen from underneath.]{\small \sl The Jolly Logic, as seen from underneath.\label{fig:jolly_bot}}  
\end{center}  
\end{figure}
\section{Jolly Logic}
One of the goals of this project was not only to provide a suitable replacement for the REX board, but to also to come up with a solution that was more versatile in terms of what the board could be used for.  The board would need a fairly large \gls{fpga} and would need the ability to program different configurations and perhaps use different peripherals via an add on module of some sort.  This section details the specifications of the Jolly Logic and the points of difference with the Mojo board that it is based on.


\subsection{Specifications}
The Atmel ATMEGA32U4 microcontoller pictured in Figure \ref{fig:atmega32u4} is the same as the one used on the Jolly Logic.  This microcontroller is in high demand for hobby projects and because of this suppliers are often out of stock.  When this board was being developed the part was unavailable and it was not until near the end of the project that the part could actually be ordered.  This type of problem was talked about in Chapter 3 and is something to be aware of when creating any board.  

The software that runs on the microcontroller is in charge of executing three key tasks.  The first is to accept new \gls{fpga} configurations over \gls{usb} and program them into the flash memory.  The second is to read any configuration stored in the flash memory and program it into the \gls{fpga} whenever the system is reset or a new setup is loaded into the flash memory.  The final task of the software is to accept communications from the \gls{fpga} in order to give it the use of the \gls{usb} port.  Additional to this the microntroller is connected to a 10 pin header for use in projects that require \gls{adc} capabilities and has enough memory that users can write custom Arduino code to use \gls{adc} features in a project.   



The Jolly Logic uses the same Spartan 6 XC6SLX9 \gls{fpga} as the Mojo.  84 of the pins are used as digital \gls{io} on the headers, the remaining \gls{io} are used for communication with the microcontroller and the flash memory.  The key specifications for the XC6SLX9 are as follows:

\begin{itemize}
\item 1,430 Slices \textemdash each Spartan-6 \gls{fpga} slice contains four \glspl{lut} and eight flip-flops.
\item 16 DSP48A1 Slices \textemdash each DSP48A1 slice contains an 18 x 18 multiplier, an adder, and an accumulator.
\item 32x18Kb Block \gls{ram} blocks \textemdash each block can also be used as two independent 9 Kb blocks
\item Two \glspl{cmt} \textemdash each \gls{cmt} contains two \glspl{dcm} and one \gls{pll}.
\item Two Memory Controller Blocks \textemdash these blocks support DDR, DDR2, DDR3, and LPDDR.
\item With the 6-input \gls{lut} architecture the theoretical number of logic cells is 9,152.
\end{itemize}

The \gls{fpga} is of ample size to contain the \gls{ip} cores for the REX board peripherals, the REX board timer, the terminal and the \gls{wramp} itself.  There is enough room on the \gls{fpga} to make improvements to the features and performance of the \gls{ip} cores that are used by the project.  If the board is used for purposes outside \gls{comp200} then the \gls{fpga} provides many useful resources that support a wide range of projects.

The flash memory used for the Jolly Logic is not the same as the memory on the Mojo.  This is because during component selection the memory needed was on back order.  It was noticed that a memory chip that was the same size, but of a lower speed was available.  As the crystal on the microcotroller only runs at 8MHz it was decided that there is no need for the higher speed memory and so the more available type was used.  The storage capacity of the memory was kept the same to allow for the largest of \gls{fpga} designs to be stored.

\begin{figure}[H]  
\begin{center}  
\includegraphics[width=6in]{jolly_top.eps}  
\caption[The Jolly Logic.]{\small \sl The Jolly Logic.\label{fig:jolly_top}}  
\end{center}  
\end{figure}

\subsection{Points of Difference}
Whilst the Jolly Logic aims to be as close to the Mojo as possible, there are still a few points of difference between the two boards.\par
One problem identified early on with the Mojo, is the location and height of the barrel jack that is intended for powering the device when \gls{usb} is not available or when a different raw voltage on a shield is required instead of the normal 5 volts the \gls{usb} supplies.  The height of the barrel jack is taller than the height of the headers that shields are connected to.  This means that a shield needs to have an area cut out from the standard area that the Mojo uses to allow for the barrel jack, otherwise a shield cannot be pushed right down into the headers without the jack being in the way.  The solution to this simple problem was to remove the barrel jack from the design as it was considered superfluous due to the existence of JP1\textemdash a two header jumper for connecting external power and is connected to the same regulators that the barrel jack would use.  On the Jolly Logic, JP1 is also intended to be used with a right-angle header so that it does not interfere with shields even when a power cable is attached to it.

The \gls{rom} memory on the Mojo is of a type that supports over 100Mhz in speed, it is assumed this is so that projects for the \gls{fpga} can utilise the \gls{rom} instead of it just storing \gls{fpga} configurations.  Because a shield has been created that adds more \gls{rom} memory as well as \gls{ram} memory, it was decided that the \gls{rom} would only be used for storing \gls{fpga} configurations.  Since the \gls{rom} will not be used for other tasks, a \gls{rom} that was faster than the microcontroller was the only speed requirement.  The microcontroller runs with a 8MHz Crystal, so much cheaper 75MHz memories were selected for the Jolly Logic.  Part of the reason for using these memories was not only price and speed, but also because of availability\textemdash The original memories that were to be used would have required a back order and the chance of project completion would have been risked.  To make the Jolly Logic as good as its predecessor for custom projects, it is advised that the memories be changed back to the higher speed type, ideally the same package type and pin ordering is used so that the \glspl{pcb} do not require modification.

The Mojo includes eight \glspl{led} that share \gls{io} with some header pins.  This enables an engineer to use the \glspl{led} for simple debugging and `hello world' style programs.  Having eight means the engineer also has enough to display an entire byte on the \glspl{led} when debugging data related problems.  Unfortunately it was proving difficult to route all eight \glspl{led} in a timely fashion.  In the interests of saving time six of the \glspl{led} were removed from the design to make the routing job easier.  The Jolly Logic can no longer display a whole byte of information on its \glspl{led}, but with the existence of the REX/WRAMP shield the seven segment displays can be used to display two bytes of information and the individual segments can be used for debugging \glspl{led} when needed.  The two remaining \glspl{led} are still useful for 'hello world' designs and simple debugging.

As mentioned in Chapter 3 the type and size of capacitors on a decoupling network can impact on its performance.  For the Jolly Logic, the type and size of capacitors were kept the same as the proven network for the Mojo.  For conservative reasons an additional 100nF capacitor of 0603 package type was also added to each branch of the network.  Because the existing network design has been proven to work, it is assumed the network for the Jolly Logic will work given its very similar design.  Another change to capacitors was for the 8MHz crystal,  it was noticed that the capacitors used are of the rating that is specified as the load capacitance in the data sheet for the crystal.  This is incorrect for external load capacitors\textemdash the correct method of calculating the load capacitors is detailed in Figure \ref{fig:crystal_caps} and was the method used for selecting the external load capacitors for the crystal.
\begin{figure}  
\begin{center} 
\setlength\fboxsep{0pt}
\setlength\fboxrule{0.5pt}
\fbox{\includegraphics[width=6in]{crystal_caps.eps}}
\caption[Algorithm for selecting load capacitors for a crystal.]{\small \sl Without using an oscilloscope, it is difficult to know the exact value for stray capacitance.  For this project 6pF was used.\label{fig:crystal_caps}}  
\end{center}  
\end{figure}

The last point of difference with the Mojo is the choice of \gls{eda} tools.  The Mojo uses a commercial tool suite for the \gls{osh} designs, which makes it less accessible to the general public as an open source design.  The Jolly Logic uses a the fully open source \gls{geda} tools, which opens up the accessibility of the designs to a greater audience.  Whilst there is a limited use version of the CadSoft Eagle tools, one of the limitations is the inability to work on 4-layer boards like the Mojo.  


\begin{figure}[H]  
\begin{center}  
\includegraphics[width=6in]{shield_top.eps}  
\caption[The REX/WRAMP shield.]{\small \sl The REX/WRAMP shield.\label{fig:shield_top}}  
\end{center}  
\end{figure}

\section{REX/WRAMP Shield}
The REX/WRAMP shield that has been created for this project includes the key peripherals needed for \gls{comp200} projects.  It also houses the \gls{ram} required for the REX board and \gls{wramp} \gls{ip} cores as well as the \gls{rom} needed to store \gls{wrampmon} \textemdash the boot loader.  In addition to catering for the legacy requirements, the shield also needed connectors to support the Verilog terminal that had been created to replace the need for physical terminals.
The first task when designing a circuit is to select the components.  Rather than copying the specifications of the REX board and using the same capacity memory, the first thing that was investigated was how much \gls{rom} is required to store \gls{wrampmon}.  A small Java program~\footnotemark[1] was created that takes a .srec file for \gls{wramp} and converts it to a .coe file used for configuring memories in a \gls{fpga}.  The resulting .coe files can then be analysed to determine a program size or used to load into block \gls{ram} for testing small \gls{wramp} programs without \gls{wrampmon}.  After compiling \gls{wrampmon} and converting it into a .coe file it was determined that the amount of \gls{rom} allocated on the REX board was of a reasonable value.  After consulting potential users, nobody seemed to have an idea on what the largest user program would be, and therefore no estimate could be obtained on what a suitable amount of \gls{ram} would be for the user programs.  As a result it was decided to provide the same amount of \gls{ram} and \gls{rom} storage space on the shield as existed on the REX board.  Once the size of the memory had been determined it was time to select the components to be used in the circuit.  The \gls{wramp} architecture uses a 32bit word size and has access to the memories using parallel communications, however the cost of memories that have a width of 32bit is very expensive when the address size is small.  The solution to this problem was to use 8bit memories and split the word into four bytes\textemdash with a different chip storing a byte each and all four chips using the same address bus.  By using this technique the cost for the bill of materials was reduced and the \gls{wramp} could use the architecture without any changes to the \gls{wramp} \gls{hdl}.  Both the \gls{ram} and the \gls{rom} uses 8bit memories for the implementation,  the total storage space is the same as the original REX board\textemdash 128K word \gls{ram} and 256K word \gls{rom}.

The REX/WRAMP shield also includes the seven segment displays, 3 buttons and 8 slide switches.  One of these buttons is for the Reset button.  The remaining buttons, the slide switches and the seven segment displays are for the peripherals on the \gls{wramp} parallel port.  All of these peripherals function in the same way as they do on the REX board, with the exception of the seven segment displays.  Due to the number of \glspl{io} available, the ability to control the decimal points on each segment of the displays has been removed.  In order to support control of the decimal points with the number of \glspl{io} available the communication would have had to change to an architecture that was multiplexing the two different displays at high speed.  Because this would have required editing of the \gls{wramp} \gls{hdl}, in the interests of saving time the option to remove the decimal points was considered the best solution.

The final feature of the REX/WRAMP shield is the connectors for the Verilog terminal.  This is simply a 6-pin Mini-DIN female socket and a 15-pin D-SUB female socket.  To save the number of \glspl{io} the VGA is wired as a single white on black display with only one signal driving the pixel\textemdash either the pixel is displayed or is blank.  This makes the Verilog terminal only require 3 signals for the VGA and 2 signals for the PS/2, making it only consume a total of 5 signals on the shield header.  This left 79 signals to control the other peripherals\textemdash an amount that proved to be the minimum required\textemdash with control of the decimal points removed.

\footnotetext[1]{See appendix C for source code.}
\begin{figure}[H]  
\begin{center}  
\includegraphics[width=6in]{shield_bot.eps}  
\caption[The REX/WRAMP shield, as seen from underneath.]{\small \sl The REX/WRAMP shield, as seen from underneath.\label{fig:shield_bot}}  
\end{center}  
\end{figure}


\section{Manufacturing and Prototype Assembly}
Once the boards had been designed and gerber files had been produced the boards needed to be manufactured by a \gls{pcb} manufacturer.  This section first covers what was learned about utilising offshore manufacturing and then goes on to discuss what techniques were used and learned during the prototype assembly. 


Once the board designs were ready, the first step was to send the designs to the chosen manufacturer for evaluation and a quote.  The company that was selected for the manufacturing process is a company based in China, called OurPCB.  The initial contact with OurPCB went OK, and as long as conversation did not stray from normal things that clients ask, the language barrier was not a problem.  However when specifics about shipping were discussed, confusion was created by Google translate.  After over 20 emails trying to explain how to merge two seperate orders into one for the purposes of shipping it was decided that a translator was needed.  With a translator using Skype to talk with the manufacturer for less than a minute all confusion was cleared up and we had reached a level of understanding.  From this experience, it is strongly advised to have access to a chinese translator when dealing with an assembly house in China.  Without a translator it is very easy to get confused due to the differences between the Chinese and English languages.  Whilst computers can translate single words between the languages, when sentences are translated the difference in grammar often causes the meaning to get lost.

When the boards eventually arrived, they came with some cross-section samples and evaluation reports.  Each board had undergone a visual inspection, a solderability test and an electrical test.  The results of these tests were contained in a two page report that read like a Warrant-of-Fitness for \glspl{pcb}.  The cross-section sample was encased in clear resin and upon inspection with a magnifying glass it was easy to identify the different layers and what layers the different vias that had been cut in half were connected to.

Once the boards had been unpackaged and the evaluation reports were read, it was time to assemble one of each prototype so that the performance could be evaluated.  The boards incorporate many \gls{smt} components that needed to be attached.  Inititally a soldering iron was used to do the soldering, however this proved difficult for the parts that have many pins.  In order to use an iron to attach components, the part needs to be tacked into place.  Once tacked in place the soldering iron is dragged accross the pins to solder them to the pads.  This method causes a fair amount of bridging and each chip needs to be cleaned up by using solder wick and flux to remove the excess solder.  Whilst this method works, it is very difficult to tack the part in the correct place.  If the part is not tacked in place correctly then the part needs to be de-soldered and moved into the correct position.  

This process is very tedious and prone to error,  so the use of solder paste and a reflow tool was investigated.  By using solder paste, the component does not need to be tacked into place and the exact placement of the part becomes less important.  Because of the surface tension of molten solder, the bubbles of solder have the effect of grabbing onto and pulling at the component pins.  This grabbing and pulling motion will align most \gls{smt} parts on their footprint, due to their low mass.  Once exact placement is taken out of the equation, the only thing to focus on is the application of the solder paste.  It is important to add enough solder paste that a good solder connection will be made, but at the same time having a small enough amount that bridging of pins is minimised.  One technique that can help with the application of solder paste was discovered by accident, it was noticed that adding paste to footprints immediately after other components had been soldered on was easier in terms of controlling the flow of paste onto the board.  The reason for this was because the board was already warmed up as a result of using the reflow tool.  The solder paste syringe has a metal tip, so when the board is warm and the tip is touching to the board, the need to squeeze the syringe is gone.  What instead happens is the solder paste is warmed up slightly by the metal tip touching the hot board.  This warmed up paste flows at a very slow and steady rate and because it is of a more runny nature, naturally spreads out and fills the pad that the paste is intended for.  When the paste is cold, the engineer has to squeeze the syringe to get the paste flowing and this results in applying too much paste or when lines of paste are being laid for the larger chips, the line is lumpy and uneven.  

Bridging still occurs using solder paste, but it is minimal and only really happens with the chips that have many pins.  For these cases where bridging occurs, it is advised to use solder wick and an iron to remove the excess paste.  When removing excess paste it is vital that plenty of flux is used, this is because the amount of solder on the component is so small that there is literally no flux left to help melt it.  If ample flux is used, the solder wick can clean up bridging quickly and easily.

Whilst solder paste is starting to sound like the only way components should be attached, it is further from the truth.  When larger sized components are to be soldered on, an iron is the best choice.  This is because the components are too heavy for the surface tension of the solder to move them into place, what actually happens is the paste will form small mountains underneath the large parts and push them up off the board.  For this reason some parts had to be removed and then resoldered using an iron.  The iron resulted in much cleaner connections than the solder paste did on large components.
